import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/*
Скрипт А. Логин в Админ панель
        1. Открыть страницу Админ панели
        2. Ввести логин, пароль и нажать кнопку Логин.
        3. После входа в систему нажать на пиктограмме пользователя в верхнем
        правом углу и выбрать опцию «Выход.»
*/

public class ScriptA {
    public static void main(String[] args) {
        WebDriver driver = CommonMethods.getDriver();
        driver.manage().window().maximize();

        //Login
        CommonMethods.prestaLogin (driver);

        //После входа в систему нажать на пиктограмме пользователя в верхнем правом углу и выбрать опцию «Выход.»
        WebElement profile_button = driver.findElement(By.id("employee_infos"));
        profile_button.click();

        CommonMethods.threadSleep(3000);

        WebElement logout = driver.findElement(By.id("header_logout"));
        logout.click();

        driver.close();
    }
}
