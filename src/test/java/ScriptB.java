import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;
import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.name;

/*
Скрипт А. Логин в Админ панель
        1. Открыть страницу Админ панели
        2. Ввести логин, пароль и нажать кнопку Логин.
        3. После входа в систему нажать на пиктограмме пользователя в верхнем
        правом углу и выбрать опцию «Выход.»
*/

public class ScriptB {
    public static void main(String[] args) {
        WebDriver driver = CommonMethods.getDriver();
        String titleOriginal, titleRefreshed;
        driver.manage().window().maximize();

       //Login
        CommonMethods.prestaLogin(driver);

        //waiting
        CommonMethods.threadSleep(3000);

        // Данный цикл у меня падает на второй итерации, так как после рефреша, айдишники элементов на странице меняются,
        //как это побороть я к сожалению не успел разобраться ((((
//        List<WebElement> menuList = driver.findElements(By.cssSelector(".maintab"));
//        for(WebElement menuItem : menuList)
//        {
//            menuItem.click();
//            titleOriginal = driver.getTitle();
//            out.println("Page title is " + titleOriginal);
//
//            driver.navigate().refresh();
//
//            //waiting
//            CommonMethods.threadSleep(3000);
//
//            titleRefreshed = driver.getTitle();
//            out.println("Page title is " + titleRefreshed);
//
//            //compare titles before and after refreshing
//            if (titleOriginal.toString().equals(titleRefreshed.toString()))
//                out.println("Page titles are the same, before and after refresh");
//            else out.println("Page titles are DIFFERENT !");
//
//            //Окончание проверки ***********************************************************************
//        }

        //Dashboard ************************************************************************
        WebElement menuDashboard = driver.findElement(id("tab-AdminDashboard"));
        menuDashboard.click();
        titleOriginal = driver.getTitle();
        out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        CommonMethods.threadSleep(3000);

        titleRefreshed = driver.getTitle();
        out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if (titleOriginal.toString().equals(titleRefreshed.toString()))
            out.println("Page titles are the same, before and after refresh");
        else out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //Заказы ***********************************************************************
        WebElement menuZakazi = driver.findElement(id("subtab-AdminParentOrders"));
        menuZakazi.click();
        titleOriginal = driver.getTitle();
        out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        CommonMethods.threadSleep(3000);

        titleRefreshed = driver.getTitle();
        out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if (titleOriginal.toString().equals(titleRefreshed.toString()))
            out.println("Page titles are the same, before and after refresh");
        else out.println("Page titles are DIFFERENT !");
        //Окончание проверки **********************************************************************

        //Каталог ************************************************************************
        WebElement menuKatalog = driver.findElement(id("subtab-AdminCatalog"));
        menuKatalog.click();
        titleOriginal = driver.getTitle();
        out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        CommonMethods.threadSleep(3000);

        titleRefreshed = driver.getTitle();
        out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if (titleOriginal.toString().equals(titleRefreshed.toString()))
            out.println("Page titles are the same, before and after refresh");
        else out.println("Page titles are DIFFERENT !");

        driver.navigate().back();

        //Окончание проверки ***********************************************************************

        //Клиенты ************************************************************************
        WebElement menuKlienti = driver.findElement(id("subtab-AdminParentCustomer"));
        menuKlienti.click();
        titleOriginal = driver.getTitle();
        out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        CommonMethods.threadSleep(3000);

        titleRefreshed = driver.getTitle();
        out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if (titleOriginal.toString().equals(titleRefreshed.toString()))
            out.println("Page titles are the same, before and after refresh");
        else out.println("Page titles are DIFFERENT !");
        //waiting
        CommonMethods.threadSleep(3000);
        //Окончание проверки ***********************************************************************

        //Служба поддержки ************************************************************************
        WebElement menuSlujbaPodderjki = driver.findElement(id("subtab-AdminParentCustomerThreads"));
        menuSlujbaPodderjki.click();
        titleOriginal = driver.getTitle();
        out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        CommonMethods.threadSleep(3000);

        titleRefreshed = driver.getTitle();
        out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if (titleOriginal.toString().equals(titleRefreshed.toString()))
            out.println("Page titles are the same, before and after refresh");
        else out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //Статистика ************************************************************************
        WebElement menuStatistika = driver.findElement(id("subtab-AdminStats"));
        menuStatistika.click();
        titleOriginal = driver.getTitle();
        out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        CommonMethods.threadSleep(3000);

        titleRefreshed = driver.getTitle();
        out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if (titleOriginal.toString().equals(titleRefreshed.toString()))
            out.println("Page titles are the same, before and after refresh");
        else out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //Modules ************************************************************************
        WebElement menuModules = driver.findElement(id("subtab-AdminParentModulesSf"));
        menuModules.click();
        titleOriginal = driver.getTitle();
        out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        CommonMethods.threadSleep(3000);

        titleRefreshed = driver.getTitle();
        out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if (titleOriginal.toString().equals(titleRefreshed.toString()))
            out.println("Page titles are the same, before and after refresh");
        else out.println("Page titles are DIFFERENT !");

        driver.navigate().back();
        //Окончание проверки ***********************************************************************

        //Design ************************************************************************
        WebElement menuDesign = driver.findElement(id("subtab-AdminParentThemes"));
        menuDesign.click();
        titleOriginal = driver.getTitle();
        out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        CommonMethods.threadSleep(3000);

        titleRefreshed = driver.getTitle();
        out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if (titleOriginal.toString().equals(titleRefreshed.toString()))
            out.println("Page titles are the same, before and after refresh");
        else out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //Доставка ************************************************************************
        WebElement menuDostavka = driver.findElement(id("subtab-AdminParentShipping"));
        menuDostavka.click();
        titleOriginal = driver.getTitle();
        out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        CommonMethods.threadSleep(3000);

        titleRefreshed = driver.getTitle();
        out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if (titleOriginal.toString().equals(titleRefreshed.toString()))
            out.println("Page titles are the same, before and after refresh");
        else out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //Способ оплаты ************************************************************************
        WebElement menuSposobOplati = driver.findElement(id("subtab-AdminParentPayment"));
        menuSposobOplati.click();
        titleOriginal = driver.getTitle();
        out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        CommonMethods.threadSleep(3000);

        titleRefreshed = driver.getTitle();
        out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if (titleOriginal.toString().equals(titleRefreshed.toString()))
            out.println("Page titles are the same, before and after refresh");
        else out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //International ************************************************************************
        WebElement menuInternational = driver.findElement(id("subtab-AdminInternational"));
        menuInternational.click();
        titleOriginal = driver.getTitle();
        out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        CommonMethods.threadSleep(3000);

        titleRefreshed = driver.getTitle();
        out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if (titleOriginal.toString().equals(titleRefreshed.toString()))
            out.println("Page titles are the same, before and after refresh");
        else out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //ShopParameters ************************************************************************
        WebElement menuShopParameters = driver.findElement(id("subtab-ShopParameters"));
        menuShopParameters.click();
        titleOriginal = driver.getTitle();
        out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        CommonMethods.threadSleep(3000);

        titleRefreshed = driver.getTitle();
        out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if (titleOriginal.toString().equals(titleRefreshed.toString()))
            out.println("Page titles are the same, before and after refresh");
        else out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        //Konfigurazija ************************************************************************
        WebElement menuKonfigurazija = driver.findElement(id("subtab-AdminAdvancedParameters"));
        menuKonfigurazija.click();
        titleOriginal = driver.getTitle();
        out.println("Page title is " + titleOriginal);

        driver.navigate().refresh();

        //waiting
        CommonMethods.threadSleep(3000);

        titleRefreshed = driver.getTitle();
        out.println("Page title is " + titleRefreshed);

        //compare titles before and after refreshing
        if (titleOriginal.toString().equals(titleRefreshed.toString()))
            out.println("Page titles are the same, before and after refresh");
        else out.println("Page titles are DIFFERENT !");
        //Окончание проверки ***********************************************************************

        driver.close();
    }
}
