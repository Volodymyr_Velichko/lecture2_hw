import com.google.common.collect.Multimaps;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ScriptC {
    public static void main(String[] args) {
        WebDriver driver = DriverManager.getConfiguredDriver("chrome");
        driver.manage().window().maximize();
        
        String categoryName= new String();
        categoryName="vvv category";

        //1. Войти в Админ Панель
        CommonMethods.prestaLogin (driver);

        //add waitings
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("subtab-AdminCatalog")));

        //2. Выбрать пункт меню Каталог -> категории и дождаться загрузки страницы управления категориями.
        WebElement catalog = driver.findElement(By.id("subtab-AdminCatalog"));

        Actions builder = new Actions(driver);
        builder.moveToElement(catalog).build().perform();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("li[data-submenu='11']")));

        WebElement categories = driver.findElement(By.cssSelector("li[data-submenu='11']"));
        categories.click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("категории")));

        //3. Нажать «Добавить категорию» для перехода к созданию новой категории

        WebElement addCategory = driver.findElement(By.xpath(".//*[text()='Добавить категорию']"));
        addCategory.click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("name_1")));


        //4. После загрузки страницы ввести название новой категории и сохранить изменения.
        //WebElement newCategoryName = driver.findElement(By.xpath("//input[@type='text'][@id='name_1']"));
        WebElement newCategoryName = driver.findElement(By.id("name_1"));
        newCategoryName.sendKeys(categoryName);

        //WebElement saveNewCategory = driver.findElement(By.xpath("//button[@id='category_form_submit_btn']"));
        WebElement saveNewCategory = driver.findElement(By.id("category_form_submit_btn"));
        saveNewCategory.click();

        //На странице управления категориями должно появиться сообщение об успешном создании категории.
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".alert-success")));

        //5. Отфильтровать таблицу категорий по имени
        //categoryFilter_name
        WebElement filterName = driver.findElement(By.name("categoryFilter_name"));
        filterName.sendKeys(categoryName);
        filterName.sendKeys(Keys.ENTER);

        wait.until(ExpectedConditions.presenceOfElementLocated(By.name("submitResetcategory")));

        String xpathFilteredCategoryName = "//td[contains(text(),'" + categoryName + "')]";
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathFilteredCategoryName)));



//        //После входа в систему нажать на пиктограмме пользователя в верхнем правом углу и выбрать опцию «Выход.»
//        WebElement profile_button = driver.findElement(By.id("employee_infos"));
//        profile_button.click();


//        WebElement logout = driver.findElement(By.id("header_logout"));
//        logout.click();
//
        driver.close();
    }
}
