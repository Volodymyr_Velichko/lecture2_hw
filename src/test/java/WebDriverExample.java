import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverExample {
    public static void main(String[] args) {
        WebDriver driver = CommonMethods.getDriver();
        driver.manage().window().maximize();
        driver.get("https://www.bing.com/");
        WebElement searchInput = driver.findElement(By.id("sb_form_q"));
        searchInput.sendKeys("Selenium");

        WebElement searchButton = driver.findElement(By.name("go"));
        searchButton.click();

        System.out.println("Page title is " + driver.getTitle());

        driver.close();
    }
}
